<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>6210Intro</title>
</head>
    <body>
        
        <?php

            $name = "Bob"; //this is now a string
            echo gettype($name);
            echo "<br>";
            echo "My name is $name";
            echo "<br>";
            echo "My name is ".$name;
            echo "<br>";
            
            $num1 = 3;
            $num2 = 4;

            if ( $num1 < $num2) {
                echo "Number 2 is greater than Number 1";
            } else {
                echo "Number 1 is greater than Number 2";
            }

            echo "<br>";
            $num1 = 3;
            $num2 = "3";

            if ( $num1 === $num2) {
                echo "The type and value are equal.";
            } else if ($num1 == $num2){
                echo "Only the value is equal.";
            } else {
                echo "Neither the value or the type is equal.";
            }


        ?>

    </body>
</html>